﻿namespace Lv3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.bFont = new System.Windows.Forms.Button();
            this.bColor = new System.Windows.Forms.Button();
            this.labelFont = new System.Windows.Forms.Label();
            this.labelBoja = new System.Windows.Forms.Label();
            this.bLoad = new System.Windows.Forms.Button();
            this.bSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(39, 89);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(608, 355);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Text|*.txt|*.rtf|*.*";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Text|*.txt|*.rtf|*.*";
            // 
            // bFont
            // 
            this.bFont.Location = new System.Drawing.Point(695, 97);
            this.bFont.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bFont.Name = "bFont";
            this.bFont.Size = new System.Drawing.Size(100, 28);
            this.bFont.TabIndex = 1;
            this.bFont.Text = "Font";
            this.bFont.UseVisualStyleBackColor = true;
            this.bFont.Click += new System.EventHandler(this.bFont_Click);
            // 
            // bColor
            // 
            this.bColor.Location = new System.Drawing.Point(695, 165);
            this.bColor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bColor.Name = "bColor";
            this.bColor.Size = new System.Drawing.Size(100, 28);
            this.bColor.TabIndex = 2;
            this.bColor.Text = "Boja";
            this.bColor.UseVisualStyleBackColor = true;
            this.bColor.Click += new System.EventHandler(this.bColor_Click);
            // 
            // labelFont
            // 
            this.labelFont.AutoSize = true;
            this.labelFont.Location = new System.Drawing.Point(695, 78);
            this.labelFont.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelFont.MaximumSize = new System.Drawing.Size(0, 20);
            this.labelFont.Name = "labelFont";
            this.labelFont.Size = new System.Drawing.Size(91, 17);
            this.labelFont.TabIndex = 3;
            this.labelFont.Text = "Odaberi font:";
            // 
            // labelBoja
            // 
            this.labelBoja.AutoSize = true;
            this.labelBoja.Location = new System.Drawing.Point(695, 145);
            this.labelBoja.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelBoja.Name = "labelBoja";
            this.labelBoja.Size = new System.Drawing.Size(94, 17);
            this.labelBoja.TabIndex = 4;
            this.labelBoja.Text = "Odaberi boju:";
            // 
            // bLoad
            // 
            this.bLoad.Location = new System.Drawing.Point(4, 0);
            this.bLoad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bLoad.Name = "bLoad";
            this.bLoad.Size = new System.Drawing.Size(60, 28);
            this.bLoad.TabIndex = 5;
            this.bLoad.Text = "Load";
            this.bLoad.UseVisualStyleBackColor = true;
            this.bLoad.Click += new System.EventHandler(this.bLoad_Click);
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(72, 0);
            this.bSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(60, 28);
            this.bSave.TabIndex = 6;
            this.bSave.Text = "Save";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 498);
            this.Controls.Add(this.bSave);
            this.Controls.Add(this.bLoad);
            this.Controls.Add(this.labelBoja);
            this.Controls.Add(this.labelFont);
            this.Controls.Add(this.bColor);
            this.Controls.Add(this.bFont);
            this.Controls.Add(this.richTextBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button bFont;
        private System.Windows.Forms.Button bColor;
        private System.Windows.Forms.Label labelFont;
        private System.Windows.Forms.Label labelBoja;
        private System.Windows.Forms.Button bLoad;
        private System.Windows.Forms.Button bSave;
    }
}

