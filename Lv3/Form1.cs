﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Lv3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bFont_Click(object sender, EventArgs e)
        {
            DialogResult result = fontDialog1.ShowDialog();

            if(result==DialogResult.OK)
            {
                Font font = fontDialog1.Font;
                this.labelFont.Text = string.Format("Odabrano:{0}", font.Name);
                this.labelFont.Font = font;
                this.richTextBox1.SelectionFont = font;
            }
        }

        private void bColor_Click(object sender, EventArgs e)
        {
            DialogResult result = colorDialog1.ShowDialog();
            if(result==DialogResult.OK)
            {
                Color color = colorDialog1.Color;
                this.labelBoja.Text = string.Format("Odabrano:{0}", "\u25A0");
                this.labelBoja.ForeColor = color;
                this.richTextBox1.SelectionColor = color;

            }
        }

        private void bLoad_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
               
                 if (openFileDialog1.FilterIndex == 1)
                 {
                     StreamReader sr = new
                     StreamReader(openFileDialog1.FileName);
                     richTextBox1.Text = sr.ReadToEnd();
                     sr.Close();
                 }
                 if (openFileDialog1.FilterIndex == 2)
                 {
                     richTextBox1.LoadFile(openFileDialog1.FileName);
                 }
            }
        }

        private void bSave_Click(object sender, EventArgs e)
        {
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                
                string name = saveFileDialog1.FileName;
                int filter = saveFileDialog1.FilterIndex;
                if (filter == 2)
                {
                    richTextBox1.SaveFile(name+".rtf", RichTextBoxStreamType.RichText);
                }
                else{
                    richTextBox1.SaveFile(name+".txt", RichTextBoxStreamType.PlainText); }
            }
        }
    }
}
